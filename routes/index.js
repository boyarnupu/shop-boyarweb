const express = require('express');
const router = express.Router();
const {getProductLink} = require('./../helper/helper');

/* GET home page. */
router.get('/', function(req, res, next) {

  let query = "SELECT * FROM `product` ORDER BY id ASC";
  db.query(query, (err, result) => {
      if (err) {
        console.log(err);
      }
      var products = result.map(function(product){
        product.link = getProductLink( req, product.slug );
        return product;
      });
      
      res.render('index', { title: 'Home', products: products });
  });
  
});

module.exports = router;
