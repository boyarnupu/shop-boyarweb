const express = require('express');
const router = express.Router();
const {getProductLink} = require('./../../helper/helper');

/* GET home page. */
router.get('/:slug', function(req, res, next) {
    let slug = req.params.slug; 
    let query = "SELECT * FROM `product` WHERE `slug`= ?";
    db.query(query, [slug], (err, result) => {
        if (err) {
            console.log(err);
        }
        product = result[0];
        product.link = getProductLink( req, slug );
        res.render('shop/single-product', { title: 'Product ' + product.name, product: product });
    });
  
});

module.exports = router;
