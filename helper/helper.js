const helper = {
    getProductLink( req, productSlug ){
        return req.protocol + '://' + req.get('host') + '/product/' + productSlug;
    }
}

module.exports = helper;